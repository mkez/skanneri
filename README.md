# skanneri

A scanner frequency list for Finland designed for the Uniden UBC125XLT.
Use with [bc125csv](https://github.com/fdev/bc125csv).

## Banks

1. LA/PR/CB
2. RHA68
3. Airband I
4. Airband II
5. Airband III
6. PMR2, 3
7. Marine I
8. Marine II, PMR3 CH 4-6
7. PMR2, 3, 4, 5, PMR446
0. Empty

## Abbreviations

### Airband
| Abbreviation | Name                                                                                                           |
|--------------|----------------------------------------------------------------------------------------------------------------|
| ACC          | [Area Control Center](https://en.wikipedia.org/wiki/Area_control_center)                                       |
| AFIS         | [Aerodrome Flight Information Service](https://en.wikipedia.org/wiki/Flight_information_service)               |
| ATIS         | [Automatic Terminal Information Service](https://en.wikipedia.org/wiki/Automatic_terminal_information_service) |
| APP          | Approach                                                                                                       |
| ARR          | Arrival                                                                                                        |
| EMG          | Emergency                                                                                                      |
| GND          | Ground                                                                                                         |
| TWR          | Tower                                                                                                          |
| Engi         | Engine flight frequency                                                                                        |
| Gen          | General use frequency                                                                                          |
| Mul          | This frequency is used by multiple airports                                                                    |

### Marine
| Abbreviation | Name                                                                           |
|--------------|--------------------------------------------------------------------------------|
| CL           | Calling channel                                                                |
| F            | Fishing                                                                        |
| GOFREP       | Gulf of Finland Reporting                                                      |
| L            | Leisure                                                                        |
| NAV          | Navigation                                                                     |
| R            | Receive channel (For duplex channels)                                          |
| SAR          | Search and Rescue                                                              |
| SOS          | Distress channel                                                               |
| T            | Transmit channel (For duplex channels)                                         |
| TR           | [Turku Radio](https://fi.wikipedia.org/wiki/Turku_Radio)                       |
| VTS          | [Vessel Traffic Service](https://en.wikipedia.org/wiki/Vessel_traffic_service) |

## Sources

* https://fi.wikibooks.org/wiki/Radiotaajuuskirja
* https://ais.fi/ais/eaip/fi/index.htm
* http://vahamartti.fi/blog/?p=832
